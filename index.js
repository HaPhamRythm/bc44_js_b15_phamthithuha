// Ex1: Tính kết quả thi

function diemKhuVuc(khuVuc) {
    if (khuVuc == "A") {
        return 2;
    } else if (khuVuc == "B") {
        return 1;
    } else if (khuVuc == "C") {
        return 0.5;
    } else if (khuVuc == "X") {
        return 0;
    } else { alert("Mã khu vực chưa được nhập đúng. Bạn hãy điền khu vực: A, B, C, X (X: nếu không thuộc khu vực ưu tiên)")}
    
}

function diemDoiTuong(doiTuong) {
    if (doiTuong == 1) {
        return 2.5;
    } else if (doiTuong == 2) {
        return 1.5;
    } else if (doiTuong == 3) {
        return 1;
    } else if (doiTuong == 0) {
        return 0;
    } else { alert("Mã đối tượng chưa được nhập đúng. Bạn hãy điền đối tượng: 1, 2, 3, 0 (0: nếu không thuộc đối tượng ưu tiên)")}
    
}

diemDoiTuong(doiTuong);

function ketQua() {
    var diemChuan = document.getElementById("diemChuan").value*1;
    var mon1 = document.getElementById("mon1").value*1;
    var mon2 = document.getElementById("mon2").value*1;
    var mon3 = document.getElementById("mon3").value*1;
    var khuVuc = document.getElementById("khuVuc").value;
    var doiTuong = document.getElementById("doiTuong").value*1;
tongDiem = mon1 + mon2 + mon3 + diemKhuVuc(khuVuc) + diemDoiTuong(doiTuong);
    if (tongDiem >= diemChuan && mon1 >0 && mon2 >0 && mon3>0) {
        document.getElementById("result1").innerHTML =`<h4 class="text-success">Chúc mừng bạn đã đậu với số điểm là: ${tongDiem}. </h4>`
    } else {
        document.getElementById("result1").innerHTML =`<h4 class="text-success">Rất tiếc bạn chưa vượt qua kỳ thi. Số điểm của bạn là: ${tongDiem}. </h4>`
    }


}

// Ex2: Tính tiền điện

function tienDien() {
    var soKw = document.getElementById("soKw").value*1;
    if (soKw <= 50){
        tongtien = soKw * 500;
    } else if (soKw <= 100) {
        tongtien = 50 * 500 + (soKw-50)*650;
    } else if (soKw <= 200) {
        tongtien = 50 * 500 + 50*650 + (soKw-100) *850;
    } else if (soKw <= 350) {
        tongtien = 50 * 500 + 50*650 + 100 *850 + (soKw-200)*1100;

    }else {
        tongtien = 50 * 500 + 50*650 + 100 *850 + 150*1100+(soKw-350)*1300;

    } 
    document.getElementById("result2").innerHTML =`<h4 class="text-success">Tổng tiền điện bạn phải thanh toán là: ${tongtien.toLocaleString()} đồng. </h4>`
}


    // Ex3: Tính thuế thu nhập cá nhân

    function pit() {
var thuNhapNam = document.getElementById("thuNhapNam").value*1;
var  phuThuoc = document.getElementById("phuThuoc").value*1;
thuNhapChiuThue = thuNhapNam - 4000000 - phuThuoc* 1600000;
var thueSuat = 0;
if (thuNhapChiuThue <=60000000) {
    thueSuat = 0.05;
} else if (thuNhapChiuThue <=120000000) {
    thueSuat = 0.1;
} else if (thuNhapChiuThue <=210000000) {
    thueSuat = 0.15;
}

else if (thuNhapChiuThue <=384000000) {
    thueSuat = 0.2;
}

else if (thuNhapChiuThue <=624000000) {
    thueSuat = 0.25;
}

else if (thuNhapChiuThue <=960000000) {
    thueSuat = 0.3;
} else {
    thueSuat = 0.35;
}


thueThuNhapCaNhan = thuNhapChiuThue * thueSuat;
document.getElementById("result3").innerHTML = `<h4 class="text-success">Thuế thu nhập cá nhân của bạn là: ${thueThuNhapCaNhan.toLocaleString()} đồng. </h4>`
    }


    // Ex4: Tính tiền cáp
// if (document.querySelector('input[name="customRadio"]:checked').value == "nhaDan") {
//     document.getElementById("soKetNoi").disabled = true;
// }

$(function(){
    $('#customRadio2').click(function(){
        document.getElementById("soKetNoi").disabled = false;
    });
 });

 $(function(){
    $('#customRadio1').click(function(){
        document.getElementById("soKetNoi").disabled = true;
    });
 });

    function tienCap() {
        var soKenh = document.getElementById("soKenh").value*1;
        var soKetNoi = document.getElementById("soKetNoi").value*1;
var loaiKH = document.querySelector('input[name="customRadio"]:checked').value;
        
        if ( loaiKH == "nhaDan") {
           tongTienCap = 4.5 + 20.5 + 7.5 * soKenh ;
    } else {tongTienCap = 15 + 75 + soKetNoi*5 + 50 * soKenh ;

    }
    
    document.getElementById("result4").innerHTML = `<h4 class="text-success">Tổng chi phí cáp của bạn là: ${tongTienCap.toLocaleString()} đồng. </h4>`
    }
